import React from 'react';
import LoginScreen from './loginScreen';

function App() {
  return (
    <div className="App">
        <LoginScreen/>
    </div>
  );
}

export default App;
